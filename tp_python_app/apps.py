from django.apps import AppConfig


class TpPythonAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tp_python_app'
