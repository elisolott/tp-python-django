from django import forms
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _
from . models import Empleado

class EmpleadoForm(forms.Form):
    dni = forms.CharField(label='Ingrese DNI', max_length=8)

class EmpleadoEditarForm(ModelForm):
    class Meta:
        model = Empleado
        fields = ['dni', 'nombre', 'apellido', 'fecha_de_nacimiento', 'sexo', 'estado_civil']
        widgets = {
            'fecha_de_nacimiento': forms.DateInput(attrs={'type': 'date'})
        }
        
    username = forms.CharField(required=True, max_length=30)
    password = forms.CharField(required=True, max_length=128)
    email = forms.EmailField(required=True)
