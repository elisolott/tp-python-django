# Generated by Django 3.2.5 on 2021-07-29 05:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tp_python_app', '0002_auto_20210728_0539'),
    ]

    operations = [
        migrations.AlterField(
            model_name='empleado',
            name='dni',
            field=models.CharField(max_length=8, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='empleado',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
