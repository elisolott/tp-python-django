from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

class Empleado(models.Model):
    dni = models.CharField(max_length=8, primary_key=True)
    usuario = models.ForeignKey(User, models.RESTRICT, blank=True, null=True)
    nombre = models.CharField(max_length=64)
    apellido = models.CharField(max_length=64)
    fecha_de_nacimiento = models.DateField(null=True, blank=False)
    
    class Sexo(models.TextChoices):
        FEMENINO = 'F', _('Femenino')
        MASCULINO = 'M', _('Masculino')
        OTRO = 'O', _('Otro')
        
    sexo = models.CharField(
        max_length=1,
        choices=Sexo.choices,
    )
        
    class EstadoCivil(models.TextChoices):
        SOLTERO = 'S', _('Soltero')    
        CASADO = 'C', _('Casado')
        DIVORCIADO = 'D', _('Divorciado')
        VIUDO = 'V', _('Viudo')
    
    estado_civil = models.CharField(
        max_length=1,
        choices=EstadoCivil.choices,
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
