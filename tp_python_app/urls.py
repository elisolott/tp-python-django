from django.urls import path
from . import views

app_name = 'tp_python_app'
urlpatterns = [
    path('', views.index, name='index'),
    path('empleados/<str:dni>', views.un_empleado, name='un_empleado'),
    path('empleados', views.lista_empleados, name='lista_empleados'),
    path('buscar-empleado', views.buscar_empleado, name='buscar_empleado'),
    path('crear-empleado', views.crear_empleado, name='crear_empleado'),
    path('editar-empleado/<str:dni>', views.editar_empleado, name='editar_empleado'),
    path('guardar-empleado', views.guardar_empleado, name='guardar_empleado'),
    path('guardar-empleado-editado', views.guardar_empleado_editado, name="guardar_empleado_editado"),
    path('borrar-empleado/<str:dni>', views.borrar_empleado, name="borrar_empleado"),
]
