from django.shortcuts import render, redirect
from django.http import HttpResponse
from . models import Empleado
from django.contrib.auth.models import User
from . forms import EmpleadoForm, EmpleadoEditarForm
from django.http import HttpResponseNotFound

def index (request):
    return render(request, "index.html")

def un_empleado (request, dni):
    if not request.user.is_authenticated:
        return HttpResponseNotFound('No tiene autorización para ver esta página. Por favor, ingrese al sistema.')
    
    try: 
        empleado = Empleado.objects.get(dni=dni)
    except Empleado.DoesNotExist:
        return HttpResponseNotFound('El empleado no existe. Por favor presione Atrás en su navegador.')
    
    context = {'empleado': empleado}
    
    return render(request, "un_empleado.html", context)

def lista_empleados (request):
    if not request.user.is_authenticated:
        return HttpResponseNotFound('No tiene autorización para ver esta página. Por favor, ingrese al sistema.')
        
    empleados = Empleado.objects.all()
    context = {'empleados': empleados}
    
    return render(request, "lista_empleados.html", context)

def buscar_empleado (request):
    if not request.user.is_authenticated:
        return HttpResponseNotFound('No tiene autorización para ver esta página. Por favor, ingrese al sistema.')
    
    if request.method == 'POST':
        return redirect('/empleados/' + request.POST.get('dni'))
    
    empleadoForm = EmpleadoForm()
    context = {'empleado_form': empleadoForm}
    
    return render(request, "buscar_empleado.html", context)

def crear_empleado (request):
    if not request.user.is_authenticated:
        return HttpResponseNotFound('No tiene autorización para ver esta página. Por favor, ingrese al sistema.')
    
    empleadoEditarForm = EmpleadoEditarForm()
    context = {'empleado_editar_form': empleadoEditarForm}
    
    return render(request, "editar_empleado.html", context)

def editar_empleado (request, dni):
    if not request.user.is_authenticated:
        return HttpResponseNotFound('No tiene autorización para ver esta página. Por favor, ingrese al sistema.')
    
    try: 
        empleado = Empleado.objects.get(dni=dni)
    except Empleado.DoesNotExist:
        return HttpResponseNotFound('El empleado no existe. Por favor presione Atrás en su navegador.')
    
    empleadoEditarForm = EmpleadoEditarForm(instance=empleado)
    empleadoEditarForm.fields['dni'].widget.attrs['readonly'] = True
    empleadoEditarForm.fields['password'].widget.attrs['readonly'] = True
    
    empleadoEditarForm.initial['username'] = empleado.usuario.username
    empleadoEditarForm.initial['password'] = empleado.usuario.password
    empleadoEditarForm.initial['email'] = empleado.usuario.email
    
    context = {
        'empleado_editar_form': empleadoEditarForm,
        'editando': True
    }
    
    return render(request, "editar_empleado.html", context)

def guardar_empleado (request):
    if not request.user.is_authenticated:
        return HttpResponseNotFound('No tiene autorización para ver esta página. Por favor, ingrese al sistema.')
    
    nuevo_empleado_form = EmpleadoEditarForm(request.POST)
    empleado_creado = nuevo_empleado_form.save(commit=False)
    
    nuevo_usuario = User.objects.create_user(
        username = request.POST.get('username'),
        password = request.POST.get('password'),
        email = request.POST.get('email'),
        first_name = request.POST.get('nombre'),
        last_name = request.POST.get('apellido')
    )
    
    empleado_creado.usuario = nuevo_usuario
    empleado_creado.save()
    
    return redirect('/empleados')

def guardar_empleado_editado (request):
    if not request.user.is_authenticated:
        return HttpResponseNotFound('No tiene autorización para ver esta página. Por favor, ingrese al sistema.')
    
    try: 
        empleado = Empleado.objects.get(dni=request.POST.get('dni'))
    except Empleado.DoesNotExist:
        return HttpResponseNotFound('El empleado no existe. Por favor presione Atrás en su navegador.')
    
    empleado_editado_form = EmpleadoEditarForm(request.POST, instance=empleado)
        
    if empleado_editado_form.is_valid():
        empleado_guardado = empleado_editado_form.save()
    else: 
        return HttpResponseNotFound('Error al editar al empleado.')
        
    empleado_guardado.usuario.username = request.POST.get('username')
    empleado_guardado.usuario.email = request.POST.get('email')
    empleado_guardado.usuario.first_name = request.POST.get('nombre')
    empleado_guardado.usuario.last_name = request.POST.get('apellido')
    
    empleado_guardado.usuario.save()
        
    return redirect('/empleados')

def borrar_empleado (request, dni):
    if not request.user.is_authenticated:
        return HttpResponseNotFound('No tiene autorización para ver esta página. Por favor, ingrese al sistema.')
    
    try: 
        empleado_borrar = Empleado.objects.get(dni=dni)
    except Empleado.DoesNotExist:
        return HttpResponseNotFound('El empleado no existe. Por favor presione Atrás en su navegador.')
    
    usuario = empleado_borrar.usuario
    
    empleado_borrar.delete()
    usuario.delete()
 
    return redirect('/empleados')
